package html_unit_basic_test;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.util.logging.Level;

public class Main {
    public Main() {
        super();
    }

    public static void main(String[] args) {
        homePage();
    }
    
    
    public static void homePage() {
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF); 
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
        try (final WebClient webClient = new WebClient()) {
            webClient.getOptions().setJavaScriptEnabled(true);
            String url = "http://htmlunit.sourceforge.net";
            System.out.println("Attempting to get page from " + url);
            final HtmlPage page = webClient.getPage(url);
            System.out.println("Success!");
           System.out.println("Connected to " + page.getBaseURL());
            final String pageAsText = page.asText();
        }
        catch(Exception e){
            System.err.println(e);
        }
    }
}
