# README #

Installing HtmlUnit for Jdeveloper

Go to http://htmlunit.sourceforge.net/

In the menu on the left click "Download"

Download the latest version

After the download is complete extract the file to any directory that you'll remember where you put it.

Start JDeveloper

In JDeveloper start up your application and select Application->Application Properties

Click "Libraries and Classpath"

Click the "Add JAR/Directory..." button

Go to where ever you extracted the HtmlUnit folder. The version I am using is htmlunit-2.26.  Go into htmlunit-2.26\lib and select all the files (Ctrl-a) with in it and click the "Open" button.

Click Ok

Test the code in this repository.